""" Init file that creates an aiohttp application """
from .factory import create_app
from .api import api


__all__ = ['app']

app = create_app()

app.add_subapp('/api', api)