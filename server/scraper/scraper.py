import time

import arxivscraper as ax
import pandas as pd

from datetime import date

categories = [
    'cs', 'econ', 'eess', 'math',
    'physics:astro-ph', 'physics:cond-mat', 'physics:gr-qc',
    'physics:hep-ex', 'physics:hep-lat', 'physics:hep-ph',
    'physics:math-ph', 'physics:nlin', 'physics:nucl-ex',
    'physics:nucl-th', 'physics:physics', 'physics:quant-ph',
    'q-bio', 'q-fin', 'stat'
]

to_scrape = ['physics:cond-mat']
first_year = 2012
last_year = 2018

for category in to_scrape:
    for year in range(first_year, last_year + 1):
        start_date = date(year=year, month=1, day=1)
        end_date = date(year=year+1, month=1, day=1)

        scraper = ax.Scraper(category=category, date_from=start_date.isoformat(), date_until=end_date.isoformat(), t=10)

        output = scraper.scrape()

        cols = ('id', 'title', 'categories', 'abstract', 'doi', 'created', 'updated', 'authors')
        dataframe = pd.DataFrame(output, columns=cols)

        dataframe.to_feather('./arxiv-data/records/{category}-{year}.feather'.format(category=category, year=year))

        time.sleep(5)
