
import os

import numpy as np
import pandas as pd

categories = [
    'cs', 'econ', 'eess', 'math',
    'physics:astro-ph', 'physics:cond-mat', 'physics:gr-qc',
    'physics:hep-ex', 'physics:hep-lat', 'physics:hep-ph',
    'physics:math-ph', 'physics:nlin', 'physics:nucl-ex',
    'physics:nucl-th', 'physics:physics', 'physics:quant-ph',
    'q-bio', 'q-fin', 'stat'
]

cache_size = 100

record_dir = './arxiv-data/records'


def record_stats(record_dir=record_dir):
    records = dict()
    for feather in os.listdir(record_dir):
        filepath = os.path.join(record_dir, feather)
        if os.path.isdir(filepath) or not feather.endswith('.feather'):
            continue

        data = pd.read_feather(filepath)
        size = data.shape[0]

        filename = feather.split('.')[0]
        category, year = filename.rsplit('-', 1)

        if category not in categories:
            continue

        if category not in records.keys():
            records[category] = dict()
        records[category][feather] = size

    return records


def random_weighted_file(category_filter=None, records=record_stats(),
                         record_dir=record_dir):
    if not category_filter:
        category_filter = records.keys()

    files = dict()
    for category in category_filter:
        if category in records.keys():
            files.update(records[category])

    if files:
        weights = np.array(list(files.values()))
        weights = weights/np.sum(weights)

        # Choose a random file, weighted by the amount of records in it
        filename = np.random.choice(list(files.keys()), p=weights)
        return os.path.join(record_dir, filename)


def random_record_from_file(filename):
    data = pd.read_feather(filename)
    if not data.empty:
        sample = data.sample(1)
        record = sample.to_dict('records')[0]
        return record


class RecordCache:

    def __init__(self, record_dir=record_dir):
        self.record_dir = record_dir
        self.record_stats = record_stats(record_dir=record_dir)
        self.cache = dict()

    def random_record(self, category_filter=None):
        random_file = random_weighted_file(
            category_filter=category_filter,
            records=self.record_stats,
            record_dir=self.record_dir,
        )

        # Load new data when the file is not in the cache or it is empty
        if random_file not in self.cache.keys() or not self.cache[random_file]:
            print('Loading new from cache')
            import sys
            sys.stdout.flush()
            data = pd.read_feather(random_file)
            if data.empty:
                return None

            size = data.shape[0]
            sample = data.sample(min(cache_size, size))
            self.cache[random_file] = sample.to_dict('records')

        return self.cache[random_file].pop()
