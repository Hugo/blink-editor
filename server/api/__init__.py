from aiohttp import web

from .records import get_record

api = web.Application()

api.router.add_routes([
    web.get('/record', get_record),
])
