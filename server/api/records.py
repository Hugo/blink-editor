from aiohttp import web

from ..records import RecordCache

record_cache = RecordCache()


async def get_record(request):
    record = record_cache.random_record()

    if not record:
        return web.json_response(
            data=dict(status=404, message='No matching records found.'),
            status=404,
        )

    return web.json_response(record)
