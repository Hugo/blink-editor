import os
import sys

from aiohttp import web

sys.path.append(os.getcwd())
from server import app  # noqa: E402


web.run_app(app)
