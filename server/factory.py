import os
from os.path import abspath, dirname

from aiohttp import web

from .database import db

STATIC_FOLDER_PATH = os.path.join(abspath(dirname(__file__)), 'static')


def create_app():
    app = web.Application()
    return app
