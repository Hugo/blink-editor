import React from 'react'

const Footer = (props) => {
  return (
    <footer className='footer'>
      <div className='container'>
        <div className='content has-text-centered'>
          <p>
            <strong>Blink Editor</strong>
          </p>
        </div>
      </div>
    </footer>
  )
}

export default Footer
