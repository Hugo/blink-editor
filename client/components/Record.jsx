import React from 'react'
import Latex from 'react-latex'

import { Card, Content } from "react-bulma-components/full";

import 'katex/dist/katex.min.css'

class Record extends React.Component {
    render() {
        const record = this.props.record !== null ? this.props.record : {'title': '', 'abstract': ''}

        return (
            <Card>
                <Card.Header>
                    <Card.Header.Title>{record.title}</Card.Header.Title>
                </Card.Header>
                <Card.Content>
                    <Content>
                        <Latex>{record.abstract}</Latex>
                    </Content>
                </Card.Content>
            </Card>
        )
    }
}

export default Record