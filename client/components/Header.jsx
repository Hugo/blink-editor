import React from 'react'
import { Link } from 'react-router-dom'

const BurgerButton = (props) => (
    <button className={'button navbar-burger' + (props.foldOut ? ' is-active' : '')}
        onClick={props.burgerClick}>
        <span />
        <span />
        <span />
    </button>
)

class NavBar extends React.Component {

    state = {
        foldOut: false,
    }

    burgerClick = () => {
        this.setState({
            foldOut: !this.state.foldOut
        })
    }

    render() {
        return (
            <nav className='navbar' role='navigation' aria-label='dropdown navigation'>

                <div className='navbar-brand'>
                    <div className='navbar-item has-text-info'>
                        <span className='icon is-medium'>
                            <i className='fas fa-book-reader fa-2x' />
                        </span>
                    </div>

                    <Link className='navbar-item has-text-info' to='/'><b>Blink Editor</b></Link>
                    <div className='navbar-item' />

                    <BurgerButton foldOut={this.props.foldOut} burgerClick={this.burgerClick} />
                </div>

                <div className={'navbar-menu' + (this.state.foldOut ? ' is-active' : '')} onClick={this.burgerClick}>
                    <div className='navbar-start'>
                    </div>

                    <div className='navbar-end'>
                        <Link className='navbar-item' to='/about'>About</Link>
                    </div>
                </div>
            </nav>
        )
    }
}

export default NavBar
