import React from 'react'

import { Button, Section, Container } from "react-bulma-components/full";

import Record from './Record.jsx'

import * as api from '../api.jsx'

class EditorPanel extends React.Component {
    state = {
        record: null
    }

    componentDidMount = () => {
        this.updateRandomRecord()
    }

    updateRandomRecord = (categories) => {
        api.get('record').then(record => {
            this.setState({
                record: record
            })
        })
    }

    render() {
        return (
            <div>
                <Record record={this.state.record} />
                <Button outlined onClick={this.updateRandomRecord} style={{'marginTop': '1em'}}>
                    Review!
                </Button>
            </div> 
        )
    }
}

export default EditorPanel