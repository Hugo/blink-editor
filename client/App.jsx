import React from 'react'
import Loadable from 'react-loadable'
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import './app.scss'

import Header from './components/Header.jsx'
import Footer from './components/Footer.jsx'
import Loading from './views/Loading.jsx'

const Home = Loadable({
  loader: () => import('./views/Home.jsx'),
  loading: Loading
})

const About = Loadable({
  loader: () => import('./views/About.jsx'),
  loading: Loading
})

class App extends React.Component {

  render () {
    return (
      <Router>
          <Header/>
          <div className={'site-content'}>
            <Switch>
              <Route path='/about' component={About} />
              <Route component={Home} />
            </Switch>
          </div>
          <Footer />
      </Router>
    )
  }
}

export default hot(module)(App)
