import React from 'react'

import EditorPanel from '../components/EditorPanel.jsx'

const Home = () => {
    return (
        <div>

            <section className='section'>

                <div className='container'>
                    <div className='content'>
                        <h1>Blink Editor</h1>
                        <p>Review journals at the blink of an eye</p>
                    </div>
                    <EditorPanel />
                </div>

            </section>

        </div>
    )
}

export default Home