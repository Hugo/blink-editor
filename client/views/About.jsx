import React from 'react'

const About = () => {
    return (
        <div>

            <section className='section'>

                <div className='container'>
                    <div className='content'>
                        <h1>About</h1>
                        <p>Some information about this project.</p>
                    </div>
                </div>

            </section>

        </div>
    )
}

export default About